import org.junit.jupiter.api.Test

private const val ELVEN_GROUP_SIZE = 3

class Day3 {

    @Test
    internal fun first() {
        readFile("3.txt")
            .map { Rucksack(listOf(it.substring(0, it.length / 2), it.substring(it.length / 2, it.length))) }
            .map { it.repeatedItems().first() }
            .map { toPriority(it) }
            .also { println(it) }
            .sum()
            .also { println(it) }
    }

    @Test
    internal fun second() {
        readFile("3.txt")
            .chunked(ELVEN_GROUP_SIZE)
            .map { Rucksack(listOf(it[0], it[1], it[2])) }
            .map { it.repeatedItems().first() }
            .map { toPriority(it) }
            .also { println(it) }
            .sum()
            .also { println(it) }
    }

    data class Rucksack(private val compartments : List<String>)
    {
        fun repeatedItems(): Set<Char> {
            return compartments
                .map { it.toCharArray().toSet() }
                .reduce { acc, items -> acc.intersect(items) }
        }
    }

    private fun toPriority(item: Char): Int = if(item.isUpperCase()) item.code - 38 else item.code - 96
}
