import org.junit.jupiter.api.Test

class Day4 {
    @Test
    internal fun first() {
        readFile("4.txt")
            .map { groupAdapter(it) }
            .map { ElvenGroup(it) }
            .also { println(it) }
            .map { it.isOneAreaWhollyEnclosedByAnother() }
            .count { it }
            .also { println("Number of pairs $it") }
    }

    @Test
    internal fun second() {
        readFile("4.txt")
            .map { groupAdapter(it) }
            .map { ElvenGroup(it) }
            .also { println(it) }
            .map { it.isOneAreaOverlappingWithAnother() }
            .count { it }
            .also { println("Number of pairs $it") }
    }

    private fun groupAdapter(entry: String): List<IntRange> = entry
        .split(",")
        .map {
            it.split("-").let { rangeEnds -> rangeEnds.first().toInt()..rangeEnds.last().toInt() }
        }

    data class ElvenGroup(private val areasToClean: List<IntRange>) {
        private fun unionOfAreas(): Set<Int> {
            return areasToClean
                .map { it.toSet() }
                .reduce { acc, areas -> acc.union(areas) }
        }

        private fun intersectionOfAreas(): Set<Int> {
            return areasToClean
                .map { it.toSet() }
                .reduce { acc, areas -> acc.intersect(areas) }
        }


        fun isOneAreaWhollyEnclosedByAnother(): Boolean = unionOfAreas().size == biggestAreaSize()

        fun isOneAreaOverlappingWithAnother(): Boolean = intersectionOfAreas().isNotEmpty()

        private fun biggestAreaSize(): Int = areasToClean
            .map { it.toList() }
            .maxBy { it.size }
            .size
    }
}
