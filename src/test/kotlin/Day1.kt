import org.junit.jupiter.api.Test

class Day1 {


    data class Elf(private val totalCalories: List<Int> = emptyList()) {
        fun plus(calories: String): Elf = this.plus(calories.toInt())
        private fun plus(calories: Int): Elf = Elf(totalCalories.plus(calories))
        fun sum() = totalCalories.sum()
    }

    @Test
    internal fun first() {
        elvesInput()
            .mapIndexed { index, elf -> elf to index }
            .maxBy { (elf) -> elf.sum() }
            .also { (elf, index) -> println(" Position ${index + 1}, total calories: ${elf.sum()}") }
    }

    @Test
    internal fun second() {
        elvesInput()
            .mapIndexed { index, elf -> elf to index }
            .sortedByDescending { (elf) -> elf.sum() }
            .take(3)
            .sumOf { (elf) -> elf.sum() }
            .also { calories -> println("total calories $calories") }
    }

    private fun elvesInput(): List<Elf> = readFile("1.txt")
        .fold(listOf(Elf())) { acc, calories ->
            when (calories) {
                "" -> acc.plusElement(Elf())
                else -> acc.mapIndexed { index, elf -> if (isLastElement(index, acc)) elf.plus(calories) else elf }
            }
    }

    private fun isLastElement(index: Int, acc: List<Elf>) = index == acc.size - 1


}


