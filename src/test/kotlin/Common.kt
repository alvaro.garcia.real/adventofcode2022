import java.io.File

fun readFile(fileName: String): List<String> =  File("src/test/resources/$fileName").readLines()
