import org.junit.jupiter.api.Test

private const val ELF_PLAY = 0
private const val HUMAN_PLAY = 1
private const val OUTCOME = 1

private const val ROCK = "ROCK"
private const val PAPER = "PAPER"
private const val SCISSORS = "SCISSORS"

class Day2 {
    @Test
    internal fun first() {
        readFile("2.txt")
            .map { it.split(" ").map { round -> adaptRounds(round) } }
            .map { Round(it[ELF_PLAY], it[HUMAN_PLAY]) }
            .map { it.figurePoints() + it.roundPoints() }
            .sum()
            .also { println("Total points $it") }
    }

    @Test
    internal fun second() {
        readFile("2.txt")
            .map { it.split(" ").map { round -> adaptRoundsWithOutcome(round) } }
            .map { PredictedRound(it[ELF_PLAY], it[OUTCOME]).toRound() }
            .map { it.figurePoints() + it.roundPoints() }
            .sum()
            .also { println("Total points $it") }
    }

    private fun adaptRounds(round: String): String {
        return when(round)
        {
            "X","A"-> ROCK
            "Y","B"-> PAPER
            "Z","C"-> SCISSORS
            else -> ""
        }
    }

    private fun adaptRoundsWithOutcome(round: String): String {
        return when(round)
        {
            "A"-> ROCK
            "B"-> PAPER
            "C"-> SCISSORS
            "X" -> LOOSE.toString()
            "Y" -> DRAW.toString()
            "Z" -> WIN.toString()
            else -> ""
        }
    }
}

data class Round(private val elfPlay: String, private val humanPlay: String) {
    fun figurePoints(): Int = when(humanPlay)
    {
        ROCK -> 1
        PAPER -> 2
        SCISSORS -> 3
        else -> 0
    }

    fun roundPoints() = roundRules[humanPlay]!![elfPlay]!!


}

data class PredictedRound(private val elfPlay: String, private val outcome:String)
{
    fun toRound() = Round(elfPlay, roundRules.filter { it.value[elfPlay] == outcome.toInt() }.keys.first())
}

private const val DRAW = 3
private const val LOOSE = 0
private const val WIN = 6

val roundRules = mapOf(
    ROCK to mapOf(ROCK to DRAW, PAPER to LOOSE, SCISSORS to WIN),
    PAPER to mapOf(ROCK to WIN, PAPER to DRAW, SCISSORS to LOOSE),
    SCISSORS to mapOf(ROCK to LOOSE, PAPER to WIN, SCISSORS to DRAW)
)
